﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA_BATTLE_of_ADMIRAL
{
    public class BattleField
    {
        public int NumOfTotalShipCellsOnField
        {
            get { return numOfTotalShipCellsOnField; }
            set { numOfTotalShipCellsOnField = value; }
        }
        public string BattleFieldName
        {
            get { return battleFieldName; }
            set { battleFieldName = value; }
        }
        public int BattleFieldCellX
        {
            get { return battleFieldcellX; }
            set { battleFieldcellX = value; }
        }
        public int BattleFieldCellY
        {
            get { return battleFieldcellY; }
            set { battleFieldcellY = value; }
        }
        public int BattleFieldHeight
        {
            get { return battleFieldHeight; }
            set { battleFieldHeight = value; }
        }
        public int BattleFieldWidth
        {
            get { return battleFieldWidth; }
            set { battleFieldWidth = value; }
        }
       
        public int BattleFieldSize
        {
            get {return battleFieldSize;}
            set { battleFieldSize = value; }
        }
        public int[,] BattleFieldArrayMatrixСells// использовать Jagged array
        {
            get { return battleFieldArrayMatrixCells; }
            set { battleFieldArrayMatrixCells = value; }
        }
        public int[,] BattleFieldArrayMatrixTurnСells
        {
            get { return battleFieldArrayMatrixTurnСells; }
            set { battleFieldArrayMatrixTurnСells = value; }
        }
        public int BattleFieldСellColor
        {
            get { return battleFieldСellColor; }
            set { battleFieldСellColor = value; }
        }
        public BattleField()
        {
            BattleFieldSize = 16;
            BattleFieldName = "BattleField";

            BattleFieldArrayMatrixСells = new int[BattleFieldSize, BattleFieldSize];
            BattleFieldArrayMatrixTurnСells = new int[BattleFieldSize, BattleFieldSize];
            for (int i = 0; i < (BattleFieldSize-1); i++)
            {
                BattleFieldArrayMatrixTurnСells[i,i] = 0;
                BattleFieldArrayMatrixСells[i,i] = 0;
            }
        }

        // поля класса, или перменные класса полебоя

        int battleFieldСellColor;
        int[,] battleFieldArrayMatrixTurnСells; // метки хода играков, выстрелы по противнику 
        int[,] battleFieldArrayMatrixCells; //занимаемые кораблями ячейки 
        int battleFieldSize; // поле 16 на 16
        int battleFieldWidth;
        int battleFieldHeight;
        int battleFieldcellY;
        int battleFieldcellX;
        string battleFieldName;
        int numOfTotalShipCellsOnField;
    }
}
