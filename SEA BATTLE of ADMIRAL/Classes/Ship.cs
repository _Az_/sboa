﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SEA_BATTLE_of_ADMIRAL
{
    public class Ship
    {

        public string ShipName
        {
            get { return shipName; }
            set { shipName = value; }
        }
        public int ShipFirstCellX
        {
            get { return shipFirstCellX; }
            set { shipFirstCellX = value; }
        }
        public int ShipFirstCellY
        {
            get { return shipFirstCellY; }
            set { shipFirstCellY = value; }
        }
        public int ShipSize
        {
            get {return shipSize;}
            set { shipSize = value; }
        }
        public int[] ShipCells // сделать методом или колекцией
        {
            get { return shipCells; }
            set { shipCells = value; }
        }
        public int ShipColor
        {
            get { return shipColor; }
            set { shipColor = value; }
        }


        public Ship()
        { 
            ShipName = "Ship";

        }

        //public Image ship;
        //Pen myshipPen = new Pen(Color.Blue, 3);
        //Rectangle myShip = new Rectangle;
       
        // поля класса корабль
        string shipName; 
        int shipFirstCellX;
        int shipFirstCellY; // поле 16 на 16
        int shipSize; //размер корабля от 2 до 5 клеток
        int[] shipCells; //занимаемые ячейки 
        int shipColor; //цвет подсветки
    }
}
