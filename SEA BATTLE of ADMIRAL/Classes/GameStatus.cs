﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEA_BATTLE_of_ADMIRAL
{
     public enum GameStatus
        {
            NewGame,
            Playing,
            DoTurn,
            StopGame,
            GameOver,
            PlayerWin,
            NPCWin,
            ExitGame
            
        }
   
}
