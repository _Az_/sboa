﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SEA_BATTLE_of_ADMIRAL
{
    public class Model
    {

        public BattleField MyField;
        public BattleField EnemyField;

        public List<Ship> MyShipList;
        public List<Ship> EnemyShipList;
       
        public List<Ship> ShipsList; // список типов кораблей

        public GameStatus gameStatus;


        public Random rnd = new Random();


        

        
  
        public Model() // конструктор класса Model
        {
            
            

            
            MyField = new BattleField(); // создаем поле для игрока 
            MyField.BattleFieldName = "PlayerField";
            
            EnemyField = new BattleField(); // поле для NPC
            EnemyField.BattleFieldName = "NPCField";

            MyShipList = new List<Ship>(); // добавляем корабли NPC 
            EnemyShipList = new List<Ship>(); // добавляем корабли NPC
            // раздаем корабли в игрокам
            ShipsList = AddShipsTypes();

            
        }
        public List<Ship> AddShipsTypes() // список возможных видов кораблей в игре
        {
            
            return new List<Ship> 

		    {
                // shipcolor = 0 флаг не установленого на поле корабля любое значение больше ноля сообщает что корабль установлен на поле
                new Ship() {ShipName = "Fregat", ShipSize = 2, ShipCells= new int[] {1,1}, ShipFirstCellX = 1, ShipFirstCellY=1, ShipColor = 0, },
                new Ship() {ShipName = "Fregat", ShipSize = 2, ShipCells= new int[] {1,1}, ShipFirstCellX = 2, ShipFirstCellY=2, ShipColor = 0, },
                new Ship() {ShipName = "Fregat", ShipSize = 2, ShipCells= new int[] {1,1}, ShipFirstCellX = 3, ShipFirstCellY=3, ShipColor = 0, },
                new Ship() {ShipName = "Fregat", ShipSize = 2, ShipCells= new int[] {1,1}, ShipFirstCellX = 4, ShipFirstCellY=4, ShipColor = 0, },
                new Ship() {ShipName = "Fregat", ShipSize = 2, ShipCells= new int[] {1,1}, ShipFirstCellX = 5, ShipFirstCellY=5, ShipColor = 0, },

                new Ship() {ShipName = "Esminec", ShipSize = 3, ShipCells= new int[] {1,1,1}, ShipFirstCellX = 6, ShipFirstCellY=6, ShipColor = 0, },
                new Ship() {ShipName = "Esminec", ShipSize = 3, ShipCells= new int[] {1,1,1}, ShipFirstCellX = 7, ShipFirstCellY=7, ShipColor = 0, },
                new Ship() {ShipName = "Esminec", ShipSize = 3, ShipCells= new int[] {1,1,1}, ShipFirstCellX = 8, ShipFirstCellY=8, ShipColor = 0, },
                new Ship() {ShipName = "Esminec", ShipSize = 3, ShipCells= new int[] {1,1,1}, ShipFirstCellX = 9, ShipFirstCellY=9, ShipColor = 0, },

                new Ship() {ShipName = "Cruiser", ShipSize = 4, ShipCells= new int[] {1,1,1,1}, ShipFirstCellX = 10, ShipFirstCellY=10, ShipColor = 0, },
                new Ship() {ShipName = "Cruiser", ShipSize = 4, ShipCells= new int[] {1,1,1,1}, ShipFirstCellX = 11, ShipFirstCellY=11, ShipColor = 0, },
                new Ship() {ShipName = "Cruiser", ShipSize = 4, ShipCells= new int[] {1,1,1,1}, ShipFirstCellX = 12, ShipFirstCellY=12, ShipColor = 0, },

                new Ship() {ShipName = "LinerShip", ShipSize = 5, ShipCells= new int[] {1,1,1,1,1}, ShipFirstCellX = 13, ShipFirstCellY=13, ShipColor = 0, },
                new Ship() {ShipName = "LinerShip", ShipSize = 5, ShipCells= new int[] {1,1,1,1,1}, ShipFirstCellX = 14, ShipFirstCellY=14, ShipColor = 0 }
            };
        }  
        public void AddShipsToShipLists(List<Ship> shipsList) // раздаем игрокам корабли из списка возможных
        {
            MyShipList = ShipsList;
            EnemyShipList = ShipsList;
        }

        public int CheckFreeLinesField(BattleField battleField, int shipLength, int cellX, int cellY) // проверка направления расположения корабля
        {
            //метод проверки не корректно обрабатывает запросы
            int fullCellsX, emptyCellsX, fullCellsY, emptyCellsY;
            fullCellsX = emptyCellsX = fullCellsY = emptyCellsY = 0;
            int currentFreeLines = 0;

            for (int k = 0; k <= shipLength; k++)
                {

                        // проверям хватит ли места под корабль в выбранном напрвлении 
                        for (int x = cellX; x < (15 - cellX); x++) // по X
                        {
                            
                                if (CheckFreeSpaceField(battleField, x, cellY)) // так же надо проверять параллельные ряды в матрице
                                {
                                    emptyCellsX++;
                                }
                           
                        }

                        for (int y = cellY; y < (15 - cellY); y++) // по У
                        {
                            
                                if (CheckFreeSpaceField(battleField, cellX, y))
                                {
                                    emptyCellsY++;
                                }
                            
                            
                        }
                        

                }

            if ((emptyCellsX > emptyCellsY) && (emptyCellsX > shipLength))
                {
                    currentFreeLines = cellX;
                 }
            else if ((emptyCellsX < emptyCellsY) && (emptyCellsY > shipLength))
                {
                    currentFreeLines = cellY;
                }
                

            

            

            return currentFreeLines;
        }

        public bool CheckFreeSpaceField(BattleField battleField, int cellX, int cellY) // проверка ячейки и прилегающих к ней 
        {
            bool CurrentFreeCell = false;
            int q, z;
            q = z = 1;
            if (battleField != null)
            {
                
                if ((cellX == 0) && (cellY == 0)) // проверка наличия занятых клеток вокруг выбранной
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                else if ((cellX == 0) && (cellY != 0))
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                else if ((cellX != 0) && (cellY == 0))
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                else if ((cellX == 15) && (cellY == 15))
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY - z] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                else if ((cellX != 15) && (cellY == 15))
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                else if ((cellX == 15) && (cellY != 15))
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY + z] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                else if ((cellY >= 1 && cellX >= 1) || (cellX <= 14 && cellY <= 14))
                {
                    if ((battleField.BattleFieldArrayMatrixСells[cellX, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX - q, cellY] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY - z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX, cellY + z] == 0)
                    && (battleField.BattleFieldArrayMatrixСells[cellX + q, cellY] == 0))
                    {
                        CurrentFreeCell = true;
                    }
                }
                
            }
            return CurrentFreeCell;
        }

        public bool PlaceAndCheck(BattleField battleField, List<Ship> ships, int cellX, int cellY) // ставим корабли на поле
        {
            // довести до ума метод расстановки 
            bool shipPlaced = false; // корабль установлен на поле
            int currentShipLength =0;
            int totalShipCellsInField = 0;
            int currentNumOfShipCellsInField = 0;
            int numOfArrangedShipsToField = 0;
            

            

            // не все корабли выводятся 
            // расстановка кривая, корабли могут стоять друг в друге
            // стоят шеренгой, т.е. нет разборса по карте



            foreach (var s in ships)
            {
                // отсортировать список так чтобы сначало устанавливались на поле самые большие корабли
                // ship.Sort();

                currentShipLength = s.ShipCells.Length; // длинна текущего корабля
                
                totalShipCellsInField += currentShipLength; // добавляем текущее количество ячеек корабля к общему количеству ячеек кораблей
                battleField.NumOfTotalShipCellsOnField = totalShipCellsInField; // записываем общее количество ячеек 

                           if (s.ShipColor == 0)
                           {
                    if (CheckFreeSpaceField(battleField, cellX, cellY))
                    {

                        if ((cellX == CheckFreeLinesField(battleField, currentShipLength, cellX, cellY))) // ставим корабль по Х
                        {
                            // перед тем как поставить карабль необходимо проверить каждую ячейку

                            for (int h = 0; h <= currentShipLength; h++)
                            {

                                battleField.BattleFieldArrayMatrixСells[cellX + h, cellY] = 1;
                                currentNumOfShipCellsInField++;
                            }
                            numOfArrangedShipsToField++;
                            s.ShipColor = 1;

                        }
                        else if ((cellY == CheckFreeLinesField(battleField, currentShipLength, cellX, cellY))) // ставим корабль по У
                        {

                            for (int h = 0; h <= currentShipLength; h++)
                            {

                                battleField.BattleFieldArrayMatrixСells[cellX, cellY + h] = 1;
                                currentNumOfShipCellsInField++;
                            }

                            numOfArrangedShipsToField++;
                            s.ShipColor = 1;

                        }
                        //else 
                        //{
                        //    PlaceShipToField(battleField, ships, cellX, cellY); // рекурсия 
                            
                        //}
                        shipPlaced = true;
                    }
                           }
                           //else if (s.ShipColor == 0)
                           //{
                           //    //77
                           //}
                }

            return shipPlaced; // возвращаем истину если корабль установлен на поле
        }
        public void FillShipsToField(BattleField battleField, List<Ship> ships) // размещаем корабли на поле
        {
            bool shipPlaced = false;
            
            int numOfArrangedShipsToField = 0;
            int cellX, cellY;
            
            cellX = cellY = 0;

            cellX = rnd.Next(0, 15);
            cellY = rnd.Next(0, 15);

            foreach (var s in ships)
            {
                while (!shipPlaced)
                {
                    shipPlaced = PlaceAndCheck(battleField, ships, cellX, cellY);
                    if (shipPlaced)
                    {
                        numOfArrangedShipsToField++;
                        
                    }

                }
            }
        }

        void ClearingBattleField(BattleField battleField) // очистка полей кораблей и полей хода
        {
            battleField.BattleFieldArrayMatrixСells = new int[16, 16];
            battleField.BattleFieldArrayMatrixTurnСells = new int[16, 16];
            for (int i = 0; i < 15; i++)
            {
                battleField.BattleFieldArrayMatrixTurnСells[i, i] = 0;
                battleField.BattleFieldArrayMatrixСells[i, i] = 0;
            }

            
        }
        
        
        public void StartNewGame()
        {
            gameStatus = GameStatus.NewGame;
            AddShipsToShipLists(ShipsList); // добваляет 14 кораблей 5 типов в список с кораблями для игроков
            FillShipsToField(MyField, MyShipList); // добавляет корабли игрока на поле
            FillShipsToField(EnemyField, EnemyShipList); // добавляет корабли игрока2 на поле
        }


        public void DoTurn()  // игровой ход 
        {

            gameStatus = GameStatus.DoTurn;

            FillShipsToField(MyField, MyShipList); // добавляет корабли игрока на поле
            FillShipsToField(EnemyField, EnemyShipList); // добавляет корабли игрока на поле
            
            //CheckField();
            //PlayerTurn();
            //CheckField();
            //EnemyTurn();
            //CheckField();

            
            
        }

        public void StopGame(BattleField battleField)  // остановить игру
        {
            ClearingBattleField(battleField);
            gameStatus = GameStatus.StopGame;

        }
        private void CheckField() // проверка попаданий по кораблям
        {
            
            gameStatus = GameStatus.Playing;

        }
        public void PlayerTurn() // отметка для стрельбы орудий
        {            
            // размечаем клетки для залпа орудий 
            //int fireAtCellX = 0;
            //int fireAtCellY = 0;
        }
        public void EnemyTurn() // ход AI
        {
            // размечаем клетки для залпа орудий 
            //int fireAtCellX = 0;
            //int fireAtCellY = 0;
        }

        public void FireAtEnemy(BattleField battleField, List<Ship> ships) // залп орудий по противнику
        {

        }


    }
}
