﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEA_BATTLE_of_ADMIRAL
{
    public partial class View //: UserControl
    {
        int currentMonitorResHeight;
        public int CurrentMonitorResHeight
        {
            get { return currentMonitorResHeight; }
            set { currentMonitorResHeight = value; }
        }
        int currentMonitorResWidth;
        public int CurrentMonitorResWidth
        {
            get { return currentMonitorResWidth; }
            set { currentMonitorResWidth = value; }
        }
        int gameFormSizeX;
        public int GameFormSizeX
        {
            get { return gameFormSizeX; }
            set { gameFormSizeX = value; }
        }
        int gameFormSizeY;
        public int GameFormSizeY
        {
            get { return gameFormSizeY; }
            set { gameFormSizeY = value; }
        }

        int numOfDecreesePix = 400;



        int gameFieldX;
        public int GameFieldX
        {
            get {return gameFieldX;}
            set { gameFieldX = value; }
        }
        int gameFieldY;
        public int GameFieldY
        {
            get {return gameFieldY;}
            set { gameFieldY = value; }
        }

        int cellXY;
        public int CellXY
        {
            get {return cellXY; }
            set {cellXY = value; }
        }

        public View(Model model)
        {
            CurrentMonitorResHeight = Screen.PrimaryScreen.Bounds.Height; // текущая высота разрешения экрана по У вниз от 0
            CurrentMonitorResWidth = Screen.PrimaryScreen.Bounds.Width; // текущая ширина разрешения экрана по Х в право от 0

            GameFormSizeX = CurrentMonitorResWidth - numOfDecreesePix; // отнимаем от полного размера экрана numOfDecreesePix пикселей  
            GameFormSizeY = CurrentMonitorResHeight - numOfDecreesePix;
            
            
            this.GameFieldX = GameFormSizeX;
            this.GameFieldY = GameFormSizeY;

            Controller_MainForm.ActiveForm.Size = new System.Drawing.Size(GameFormSizeX, GameFormSizeY); // изменяем размер контрола View на главной форме 

            CellXY = GameFieldX / 40; // размер клетки поля и размера клетки корабля
            
            InitializeComponent();
        }

        public void DrawShips(BattleField battlefield, PaintEventArgs a) // прорисовка кораблей на поле из матрицы BattleFieldArrayMatrixСells
        {
            int stepX = 0;
            int stepY = 0;



            if (battlefield.BattleFieldName == "PlayerField") // рисуем корабли игрока зеленым в первом левом поле если текущее поле с именем поля игрока
            {

                for (int i = 0; i <= 15; i++)
                {

                    for (int j = 0; j <= 15; j++)
                    {
                        if (battlefield.BattleFieldArrayMatrixСells[i, j] == 1)
                        {
                            stepX = (((i + 1) * CellXY) + 5);
                            stepY = (((j + 1) * CellXY) + 5);
                            a.Graphics.DrawRectangle(new Pen(Color.Green, 5), stepX, stepY, CellXY, CellXY);

                        }

                    }

                }

            }
            else // рисуем красным корабли противника
            {


                for (int i = 0; i <= 15; i++)
                {

                    for (int j = 0; j <= 15; j++)
                    {
                        if (battlefield.BattleFieldArrayMatrixСells[i, j] == 1)
                        {
                            stepX = (((i + 1) * CellXY)) + CellXY * 20;
                            stepY = (((j + 1) * CellXY) + 5);
                            a.Graphics.DrawRectangle(new Pen(Color.Red, 5), stepX, stepY, CellXY, CellXY);

                        }

                    }

                }
            }

        }
        public void DrawFields(PaintEventArgs a) // рисуем клетки поля
        {
            

            // начало отрисовки ячеек
            int stepX = 5;
            int stepY = 5;

            for (int i = 1; i <= 16; i++)
            {
                for (int j = 1; j <= 16; j++)
                {
                    a.Graphics.DrawRectangle(new Pen(Color.Blue, 1), stepX, stepY, CellXY, CellXY);
                   stepX += CellXY;
                   
                }
                stepX = 5;
                stepY += CellXY;
                
                
            }

            
            stepY = 5;
            stepX = CellXY*20;
            for (int i = 1; i <= 16; i++)
            {
                
                for (int j = 1; j <= 16; j++)
                {

                    a.Graphics.DrawRectangle(new Pen(Color.Blue, 1), stepX, stepY, CellXY, CellXY);
                    stepX += CellXY;

                }

                stepX = CellXY * 20;
                stepY += CellXY;
                

            }

            // границы полей толстые серые линии вокруг поля клеток 

            a.Graphics.DrawRectangle(new Pen(Color.Gray, 7), 5, 5, CellXY * 16, CellXY * 16);
            a.Graphics.DrawRectangle(new Pen(Color.Gray, 7), CellXY * 20, 5, CellXY * 16, CellXY * 16);

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            DrawFields(e);
            DrawShips(model.MyField, e);
            DrawShips(model.EnemyField, e);
            Invalidate(); // перерисовываем графику на форме 


        }

        //private void View_MouseClick(object sender, MouseEventArgs e)
        //{
            
        //}

        //private void View_MouseHover(object sender, EventArgs e)
        //{

        //}

        //private void View_Click(object sender, EventArgs e)
        //{

        //}


    }
}
