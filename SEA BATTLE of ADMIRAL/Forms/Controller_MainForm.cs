﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SEA_BATTLE_of_ADMIRAL
{
    public partial class Controller_MainForm : Form
    {
        int formSizeX; // размер формы по Х
        public int FormSizeX
        {
            get { return formSizeX; }
            set { formSizeX = value; }
        }
        int formSizeY; // размер формы по У
        public int FormSizeY
        {
            get { return formSizeY; }
            set { formSizeY = value; }
        }
     
        float textSize; // размер текста на экране
        public float TextSize
        {
            get{return textSize;}
            set { textSize = value; }
        }

        int upperSpace; // отступ
        public int UpperSpace
        {
            get { return upperSpace; }
            set { upperSpace = value; }
        }

        
        

        public Controller_MainForm()
        {
            

            Model model = new Model();
            View view = new View();

            FormSizeX = view.GameFormSizeX;
            FormSizeY = view.GameFormSizeY;
            

            TextSize = FormSizeY / 100;

            UpperSpace = (FormSizeX / 10) / 3;
            InitializeComponent();
            FormCustomization();
            model.StartNewGame();
        }

        public void FormCustomization()
        {
            this.ClientSize = new System.Drawing.Size(FormSizeX, FormSizeY);
            this.Start_btn.Size = new System.Drawing.Size(FormSizeX / 10, FormSizeY / 20);
            this.Start_btn.Location = new System.Drawing.Point(1, 1);
            
            this.Start_btn.Text = "НОВАЯ ИГРА";

            this.Start_btn.Font = new System.Drawing.Font("Tahoma", TextSize, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            //view.BackColor = System.Drawing.Color.Black;

            view.Location = new Point(10, upperSpace);
            this.MaximumSize = new System.Drawing.Size(FormSizeX, FormSizeY);
            this.MinimumSize = new System.Drawing.Size(FormSizeX, FormSizeY);
            this.Controls.Add(this.view);
        }


        private void Start_btn_Click(object sender, EventArgs e)
        {

            
            if ((model.gameStatus == GameStatus.NewGame) || (model.gameStatus == GameStatus.StopGame))
            {
                this.Text = "Start New Game";
                model.DoTurn();
            }

            else if (model.gameStatus == GameStatus.DoTurn)
            {
                model.StopGame(model.MyField);
                model.StopGame(model.EnemyField);
                this.Text = "Playing";
                model.DoTurn();
            
               
            }

          
            

        }

       

        



    }
}
